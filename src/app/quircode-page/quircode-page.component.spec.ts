import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuircodePageComponent } from './quircode-page.component';

describe('QuircodePageComponent', () => {
  let component: QuircodePageComponent;
  let fixture: ComponentFixture<QuircodePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuircodePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuircodePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

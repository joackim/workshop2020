import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { CustomerAccountPageComponent } from './customer-account-page/customer-account-page.component';
import { EditProfilePageComponent } from './edit-profile-page/edit-profile-page.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { QuircodePageComponent } from './quircode-page/quircode-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    CustomerAccountPageComponent,
    EditProfilePageComponent,
    OrderPageComponent,
    QuircodePageComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
